---
title: "Studio x86_64"
subtitle: ""
# meta description
type: "os/download/home/x86_64"
description: "Best for standard needs"
image: images/thumbnail.jpg
opengraph:
  image: images/thumbnail.jpg
draft: false
download:
  enable: true
  content: "Studio Edition for professional needs. There are complete graphic design applications. And supports non-free applications such as nvidia, zoom, discord and others.<br><br>
  LangitKetujuh OS Studio Edition is free of charge and does not generate any direct revenue. It is funded by advertising, sponsorships and donations and is financially supported by its own user community. Support us to continue growing."
  button:
    enable: true
    label_amd: AMD
    link_amd: https://is3.cloudhost.id/langitketujuh/iso/langitketujuh-live-x86_64-20250101-studio-amd.iso
    label_intel: INTEL
    link_intel: https://is3.cloudhost.id/langitketujuh/iso/langitketujuh-live-x86_64-20250101-studio-intel.iso
    label_amd_nvidia: AMD+NVIDIA
    link_amd_nvidia: https://is3.cloudhost.id/langitketujuh/iso/langitketujuh-live-x86_64-20250101-studio-amd-nvidia.iso
    label_intel_nvidia: INTEL+NVIDIA
    link_intel_nvidia: https://is3.cloudhost.id/langitketujuh/iso/langitketujuh-live-x86_64-20250101-studio-intel-nvidia.iso
    sha256sum: https://is3.cloudhost.id/langitketujuh/iso/sha256sum.txt
  mirror:
    enable: false
    label_mirror_auto: Mirror Auto
    link_mirror_auto:
    label_mirror_1: Mirror 1
    link_mirror_1:
    label_mirror_2: Mirror 2
    link_mirror_2:
---
